From iris_examples.logrel.F_mu_ref_conc Require Export logrel_unary.
From iris_examples.logrel.F_mu_ref_conc Require Import rules.
From iris.base_logic Require Export big_op invariants.
From iris.program_logic Require Export lifting.
From iris.proofmode Require Import tactics.

Definition log_typed `{heapIG Σ} (Γ : list type) (e : expr) (τ : type) := ∀ Δ vs,
  env_Persistent Δ →
  ⟦ Γ ⟧* Δ vs ⊢ ⟦ τ ⟧ₑ Δ e.[env_subst vs].
Notation "Γ ⊨ e : τ" := (log_typed Γ e τ) (at level 74, e, τ at next level).

Section typed_interp.
  Context `{heapIG Σ}.
  Notation D := (valC -n> iProp Σ).
  Implicit Types e : expr.

  Local Tactic Notation "use_bind" uconstr(ctx) ident(v) constr(Hv) uconstr(Hp) :=
    iApply (wp_bind (fill [ctx]));
    iApply (wp_wand with "[-]"); [iApply Hp; trivial|]; cbn;
    iIntros (v) Hv.

  Lemma compat_var Γ x τ (Hx : Γ !! x = Some τ) : Γ ⊨ Var x : τ.
  Proof.
    iIntros (Δ vs HΔ) "#HΓ /=".
    iDestruct (interp_env_Some_l with "HΓ") as (v) "[% ?]"; first done.
    rewrite /env_subst. simplify_option_eq. by iApply wp_value.
  Qed.

  Lemma compat_unit Γ : Γ ⊨ Unit : TUnit.
  Proof. iIntros (Δ vs HΔ) "#HΓ /=". by iApply wp_value. Qed.

  Lemma compat_nat Γ n : Γ ⊨ #n n : TNat.
  Proof. iIntros (Δ vs HΔ) "#HΓ /=". iApply wp_value. simpl. eauto. Qed.

  Lemma compat_bool Γ b : Γ ⊨ #♭ b : TBool.
  Proof. iIntros (Δ vs HΔ) "#HΓ /=". iApply wp_value. simpl. eauto. Qed.

  Lemma compat_binop Γ op e1 e2 :
    Γ ⊨ e1 : TNat → Γ ⊨ e2 : TNat → Γ ⊨ BinOp op e1 e2 : binop_res_type op.
  Proof.
    iIntros (e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (BinOpLCtx _ _) v "#Hv" e1Typed.
    use_bind (BinOpRCtx _ _) v' "# Hv'" e2Typed.
    iDestruct "Hv" as (n) "%"; iDestruct "Hv'" as (n') "%"; simplify_eq/=.
    iApply wp_pure_step_later; auto. iNext. iApply wp_value.
    destruct op; simpl; try destruct eq_nat_dec;
      try destruct le_dec; try destruct lt_dec; eauto 10.
  Qed.

  Lemma compat_pair Γ e1 e2 τ1 τ2 :
    Γ ⊨ e1 : τ1 → Γ ⊨ e2 : τ2 → Γ ⊨ Pair e1 e2 : TProd τ1 τ2.
  Proof.
    iIntros (e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (PairLCtx _) v "#Hv" e1Typed.
    use_bind (PairRCtx _) v' "# Hv'" e2Typed.
    iApply wp_value; eauto.
  Qed.

  Lemma compat_fst Γ e τ1 τ2 : Γ ⊨ e : TProd τ1 τ2 → Γ ⊨ Fst e : τ1.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind FstCtx v "# Hv" eTyped; cbn.
    iDestruct "Hv" as (w1 w2) "#[% [H2 H3]]"; subst.
    iApply wp_pure_step_later; auto. by iApply wp_value.
  Qed.

  Lemma compat_snd Γ e τ1 τ2 : Γ ⊨ e : TProd τ1 τ2 → Γ ⊨ Snd e : τ2.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind SndCtx v "# Hv" eTyped; cbn.
    iDestruct "Hv" as (w1 w2) "#[% [H2 H3]]"; subst.
    iApply wp_pure_step_later; auto. by iApply wp_value.
  Qed.

  Lemma compat_injl Γ e τ1 τ2 : Γ ⊨ e : τ1 → Γ ⊨ InjL e : TSum τ1 τ2.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind InjLCtx v "# Hv" eTyped; cbn.
    iApply wp_value; eauto.
  Qed.

  Lemma compat_injr Γ e τ1 τ2 : Γ ⊨ e : τ2 → Γ ⊨ InjR e : TSum τ1 τ2.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind InjRCtx v "# Hv" eTyped; cbn.
    iApply wp_value; eauto.
  Qed.

  Lemma compat_case Γ e0 e1 e2 τ1 τ2 τ3
      (e1Closed : ∀ f, e1.[upn (S (length Γ)) f] = e1)
      (e2Closed : ∀ f, e2.[upn (S (length Γ)) f] = e2) :
    Γ ⊨ e0 : TSum τ1 τ2 → τ1 :: Γ ⊨ e1 : τ3 → τ2 :: Γ ⊨ e2 : τ3 →
    Γ ⊨ Case e0 e1 e2 : τ3.
  Proof.
    iIntros (e0Typed e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (CaseCtx _ _) v "#Hv" e0Typed; cbn.
    iDestruct (interp_env_length with "HΓ") as %?.
    iDestruct "Hv" as "[Hv|Hv]"; iDestruct "Hv" as (w) "[% Hw]"; simplify_eq/=.
    - iApply wp_pure_step_later; auto 1 using to_of_val; asimpl. iNext.
      erewrite n_closed_subst_head_simpl by naive_solver.
      iApply (e1Typed Δ (w :: vs)). iApply interp_env_cons; auto.
    - iApply wp_pure_step_later; auto 1 using to_of_val; asimpl. iNext.
      erewrite n_closed_subst_head_simpl by naive_solver.
      iApply (e2Typed Δ (w :: vs)). iApply interp_env_cons; auto.
  Qed.

  Lemma compat_if Γ e0 e1 e2 τ :
    Γ ⊨ e0 : TBool → Γ ⊨ e1 : τ → Γ ⊨ e2 : τ → Γ ⊨ If e0 e1 e2 : τ.
  Proof.
    iIntros (e0Typed e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (IfCtx _ _) v "#Hv" e0Typed; cbn.
    iDestruct "Hv" as ([]) "%"; subst; simpl;
      [iApply wp_pure_step_later .. ]; auto; iNext;
      [iApply e1Typed| iApply e2Typed]; auto.
  Qed.

  Lemma compat_rec Γ e τ1 τ2
      (eClosed : ∀ f, e.[upn (S (S (length Γ))) f] = e) :
    TArrow τ1 τ2 :: τ1 :: Γ ⊨ e : τ2 → Γ ⊨ Rec e : TArrow τ1 τ2.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    iApply wp_value. simpl. iAlways. iLöb as "IH". iIntros (w) "#Hw".
    iDestruct (interp_env_length with "HΓ") as %?.
    iApply wp_pure_step_later; auto 1 using to_of_val. iNext.
    asimpl. change (Rec _) with (of_val (RecV e.[upn 2 (env_subst vs)])) at 2.
    erewrite n_closed_subst_head_simpl_2 by naive_solver.
    iApply (eTyped Δ (_ :: w :: vs)).
    iApply interp_env_cons; iSplit; [|iApply interp_env_cons]; auto.
  Qed.

  Lemma compat_app Γ e1 e2 τ1 τ2 :
    Γ ⊨ e1 : TArrow τ1 τ2 → Γ ⊨ e2 : τ1 → Γ ⊨ App e1 e2 : τ2.
  Proof.
    iIntros (e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (AppLCtx _) v "#Hv" e1Typed.
    use_bind (AppRCtx _) w "#Hw" e2Typed.
    by iApply "Hv".
  Qed.

  Lemma compat_tlam Γ e τ :
    subst (ren (+1)) <$> Γ ⊨ e : τ → Γ ⊨ TLam e : TForall τ.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    iApply wp_value.
    iAlways; iIntros (τi) "%". iApply wp_pure_step_later; auto; iNext.
    iApply eTyped. by iApply interp_env_ren.
  Qed.

  Lemma compat_tapp Γ e τ τ' : Γ ⊨ e : TForall τ → Γ ⊨ TApp e : τ.[τ'/].
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind TAppCtx v "#Hv" eTyped; cbn.
    iApply wp_wand_r; iSplitL; [iApply ("Hv" $! (⟦ τ' ⟧ Δ)); iPureIntro; apply _|]; cbn.
    iIntros (w) "?". by iApply interp_subst.
  Qed.

  Lemma compat_pack Γ e τ τ' : Γ ⊨ e : τ.[τ'/] → Γ ⊨ Pack e : TExist τ.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind PackCtx v "#Hv" eTyped; cbn.
    iApply wp_value. iAlways. iExists v, (⟦ τ' ⟧ Δ).
    iSplit; first done. iSplit; first by iPureIntro=>?; apply _.
    by iApply interp_subst.
  Qed.

  Lemma compat_unpack Γ e0 e1 τ τ1
      (e1Closed : ∀ f, e1.[upn (S (length Γ)) f] = e1) :
    Γ ⊨ e0 : TExist τ → τ :: (subst (ren(+1)) <$> Γ) ⊨ e1 : τ1.[ren(+1)] →
    Γ ⊨ Unpack e0 e1 : τ1.
  Proof.
    iIntros (e0Typed e1Typed Δ vs HΔ) "#HΓ /=".
    use_bind (UnpackCtx _) v "#Hv" e0Typed; cbn.
    iDestruct "Hv" as (w τi) "(% & % & Hw)"; subst.
    iApply wp_pure_step_later; auto 1 using to_of_val; asimpl.
    rewrite -/of_val. iNext.
    iDestruct (interp_env_length with "HΓ") as %?.
    erewrite n_closed_subst_head_simpl; [|done|solve_length].
    iApply interp_expr_unused.
    iApply (e1Typed (τi :: Δ) (w :: vs)).
    iApply interp_env_cons. rewrite (interp_env_ren Δ Γ vs τi).
    by iFrame "#".
  Qed.

  Lemma compat_fold Γ e τ : Γ ⊨ e : τ.[TRec τ/] → Γ ⊨ Fold e : TRec τ.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    iApply (wp_bind (fill [FoldCtx]));
      iApply wp_wand_l; iSplitL; [|iApply (eTyped Δ vs); auto].
    iIntros (v) "#Hv". iApply wp_value.
    rewrite /= -interp_subst fixpoint_unfold /=.
    iAlways; eauto.
  Qed.

  Lemma compat_unfold Γ e τ : Γ ⊨ e : TRec τ → Γ ⊨ Unfold e : τ.[TRec τ/].
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    iApply (wp_bind (fill [UnfoldCtx]));
      iApply wp_wand_l; iSplitL; [|iApply eTyped; auto].
    iIntros (v) "#Hv". rewrite /= fixpoint_unfold.
    change (fixpoint _) with (⟦ TRec τ ⟧ Δ); simpl.
    iDestruct "Hv" as (w) "#[% Hw]"; subst.
    iApply wp_pure_step_later; cbn; auto using to_of_val.
    iNext. iApply wp_value. by iApply interp_subst.
  Qed.

  Lemma compat_fork Γ e : Γ ⊨ e : TUnit → Γ ⊨ Fork e : TUnit.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    iApply wp_fork. iNext; iSplitL; trivial.
    iApply wp_wand_l. iSplitL; [|iApply eTyped; auto]; auto.
  Qed.

  Lemma compat_alloc Γ e τ : Γ ⊨ e : τ → Γ ⊨ Alloc e : Tref τ.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind AllocCtx v "#Hv" eTyped; cbn. iClear "HΓ". iApply wp_fupd.
    iApply wp_alloc; auto 1 using to_of_val.
    iNext; iIntros (l) "Hl".
    iMod (inv_alloc _ with "[Hl]") as "HN";
      [| iModIntro; iExists _; iSplit; trivial]; eauto.
  Qed.

  Lemma compat_load Γ e τ : Γ ⊨ e : Tref τ → Γ ⊨ Load e : τ.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind LoadCtx v "#Hv" eTyped; cbn. iClear "HΓ".
    iDestruct "Hv" as (l) "[% #Hv]"; subst.
    iApply wp_atomic.
    iInv (logN .@ l) as (w) "[Hw1 #Hw2]" "Hclose".
    iApply (wp_load with "Hw1").
    iModIntro. iNext.
    iIntros "Hw1". iMod ("Hclose" with "[Hw1 Hw2]"); eauto.
  Qed.

  Lemma compat_store Γ e1 e2 τ :
    Γ ⊨ e1 : Tref τ → Γ ⊨ e2 : τ → Γ ⊨ Store e1 e2 : TUnit.
  Proof.
    iIntros (e1Typed e2Typed Δ vs HΔ) "#HΓ /=".
    use_bind (StoreLCtx _) v "#Hv" e1Typed; cbn.
    use_bind (StoreRCtx _) w "#Hw" e2Typed; cbn. iClear "HΓ".
    iDestruct "Hv" as (l) "[% #Hv]"; subst.
    iApply wp_atomic.
    iInv (logN .@ l) as (z) "[Hz1 #Hz2]" "Hclose".
    iApply (wp_store with "Hz1"); auto using to_of_val.
    iModIntro. iNext.
    iIntros "Hz1". iMod ("Hclose" with "[Hz1 Hz2]"); eauto.
  Qed.

  Lemma compat_cas Γ e1 e2 e3 τ :
    EqType τ → Γ ⊨ e1 : Tref τ → Γ ⊨ e2 : τ → Γ ⊨ e3 : τ →
    Γ ⊨ CAS e1 e2 e3 : TBool.
  Proof.
    iIntros (? e1Typed e2Typed e3Typed Δ vs HΔ) "#HΓ /=".
    use_bind (CasLCtx _ _) v1 "#Hv1" e1Typed; cbn.
    use_bind (CasMCtx _ _) v2 "#Hv2" e2Typed; cbn.
    use_bind (CasRCtx _ _) v3 "#Hv3" e3Typed; cbn. iClear "HΓ".
    iDestruct "Hv1" as (l) "[% Hv1]"; subst.
    iApply wp_atomic.
    iInv (logN .@ l) as (w) "[Hw1 #Hw2]" "Hclose".
    destruct (decide (v2 = w)) as [|Hneq]; subst.
    + iApply (wp_cas_suc with "Hw1"); auto using to_of_val.
      iModIntro. iNext.
      iIntros "Hw1". iMod ("Hclose" with "[Hw1 Hw2]"); eauto.
    + iApply (wp_cas_fail with "Hw1"); auto using to_of_val.
      iModIntro. iNext.
      iIntros "Hw1". iMod ("Hclose" with "[Hw1 Hw2]"); eauto.
  Qed.

  Lemma compat_fai Γ e : Γ ⊨ e : Tref TNat → Γ ⊨ FAI e : TNat.
  Proof.
    iIntros (eTyped Δ vs HΔ) "#HΓ /=".
    use_bind FAICtx v "#Hv" eTyped; cbn. iClear "HΓ".
    iDestruct "Hv" as (l) "[% #Hv]"; subst.
    iApply wp_atomic.
    iInv (logN .@ l) as (w) "[Hw1 >Hw2]" "Hclose".
    iDestruct "Hw2" as (n) "%"; subst.
    iApply (wp_fai with "Hw1"). iModIntro. iNext.
    iIntros "Hw1". iMod ("Hclose" with "[Hw1]"); eauto.
    iExists (#nv S n); eauto.
  Qed.

  Theorem fundamental Γ e τ : Γ ⊢ₜ e : τ → Γ ⊨ e : τ.
  Proof.
    induction 1; iIntros (Δ vs HΔ) "#HΓ /=".
    - by iApply compat_var.
    - by iApply compat_unit.
    - by iApply compat_nat.
    - by iApply compat_bool.
    - by iApply compat_binop.
    - by iApply compat_pair.
    - by iApply compat_fst.
    - by iApply compat_snd.
    - by iApply compat_injl.
    - by iApply compat_injr.
    - by iApply compat_case; eauto;
        match goal with H : _ |- _ => eapply (typed_n_closed _ _ _ H) end.
    - by iApply compat_if.
    - by iApply compat_rec; eauto;
        match goal with H : _ |- _ => eapply (typed_n_closed _ _ _ H) end.
    - by iApply compat_app.
    - by iApply compat_tlam.
    - by iApply compat_tapp.
    - by iApply compat_pack.
    - iApply compat_unpack; eauto. move: H0_0 => /typed_n_closed.
      by rewrite cons_length fmap_length.
    - by iApply compat_fold.
    - by iApply compat_unfold.
    - by iApply compat_fork.
    - by iApply compat_alloc.
    - by iApply compat_load.
    - by iApply compat_store.
    - by iApply compat_cas.
    - by iApply compat_fai.
  Qed.
End typed_interp.
