# IRIS EXAMPLES

This is a fork of the
[iris-examples](https://gitlab.mpi-sws.org/FP/iris-examples.git)
repository formalizing the safety proof sketched in Derek Dreyer's
POPL '18 talk.

## Prerequisites

This version is known to compile with:

 - Coq 8.6.1 / 8.7.1
 - Ssreflect 1.6.4
 - A development version of [Iris](https://gitlab.mpi-sws.org/FP/iris-coq/)
 - The coq86-devel branch of [Autosubst](https://github.com/uds-psl/autosubst)

## Building from source

When building from source, we recommend to use opam (1.2.2 or newer) for
installing the dependencies.  This requires the following two repositories:

    opam repo add coq-released https://coq.inria.fr/opam/released
    opam repo add iris-dev https://gitlab.mpi-sws.org/FP/opam-dev.git

Once you got opam set up, run `make build-dep` to install the right versions
of the dependencies.

Run `make -jN` to build the full development, where `N` is the number of your
CPU cores.

To update, do `git pull`.  After an update, the development may fail to compile
because of outdated dependencies.  To fix that, please run `opam update`
followed by `make build-dep`.

## Derek's POPL '18 talk

This repository contains the safety proof sketched in Derek Dreyer's
POPL '18 talk. It is derived from [earlier
work](https://gitlab.mpi-sws.org/FP/iris-examples.git) accompanying
the [Iris proof mode
paper](http://doi.acm.org/10.1145/3093333.3009855).

The language, type system, and logical relation we use are defined in

	theories/logrel/F_mu_ref_conc/lang.v
	theories/logrel/F_mu_ref_conc/typing.v
	theories/logrel/F_mu_ref_conc/logrel_unary.v

The language is System F with recursive types, references,
concurrency, and an unsafe expression `assert e` that gets stuck
unless `e` evaluates to `true`. (An additional file,
`theories/logrel/F_mu_ref_conc/rules.v`, derives some Iris proof rules
from the language's operational semantics.)

The relevant metatheorems are proved in

	theories/logrel/F_mu_ref_conc/fundamental_unary.v
	theories/logrel/F_mu_ref_conc/soundness_unary.v

These are the fundamental theorem of logical relations and a semantic
proof of type soundness.

The symbol ADT is verified in

	theories/logrel/F_mu_ref_conc/examples/symbol.v

The goal is to show that a syntactically unsafe implementation of
symbols is semantically safe:

	pack <int,
		let c = ref 0 in
		<λ_. c++,
		 λ_. assert(s < *c)>
	as ∃α. (unit → α) × (α → unit)
	∈ E[∃α. (unit → α) × (α → unit)]

This is lemma `type_symbol` in that file.

## Case Studies

This repository also contains several case studies, disabled in
`_CoqProject`:

* [barrier](theories/barrier): Implementation and proof of a barrier as
  described in ["Higher-Order Ghost State"](http://doi.acm.org/10.1145/2818638).
* [logrel](theories/logrel): Logical relations from the
  [IPM paper](http://doi.acm.org/10.1145/3093333.3009855):
  - STLC
      - Unary logical relations proving type safety
  - F_mu (System F with recursive types)
      - Unary logical relations proving type safety
  - F_mu_ref (System F with recursive types and references)
      - Unary logical relations proving type safety
      - Binary logical relations for proving contextual refinements
  - F_mu_ref_conc (System F with recursive types, references and concurrency)
      - Unary logical relations proving type safety
      - Binary logical relations for proving contextual refinements
          - Proof of refinement for a pair of fine-grained/coarse-grained
            concurrent counter implementations
          - Proof of refinement for a pair of fine-grained/coarse-grained
            concurrent stack implementations
* [spanning-tree](theories/spanning_tree): Proof of a concurrent spanning tree
  algorithm.
* [lecture-notes](theories/lecture_notes): Coq examples for the
  [Iris lecture notes](http://iris-project.org/tutorial-material.html).

If you are interested in these, we recommend that you find them in
[the original iris-examples
repository](https://gitlab.mpi-sws.org/FP/iris-examples.git). To
verify safety for the symbol ADT, we extended the language in
`F_mu_rec_conc` with existential types, but we neither updated that
language's binary logical relation nor proved that relation compatible
with `pack` and `unpack`. This means that examples that rely on the
binary logical relation for `F_mu_rec_conc` no longer compile.
(Everything else would compile if enabled in `_CoqProject`.)

## For Developers: How to update the Iris dependency

* Do the change in Iris, push it.
* Wait for CI to publish a new Iris version on the opam archive, then run
  `opam update iris-dev`.
* In iris-examples, change the `opam` file to depend on the new version.
* Run `make build-dep` (in iris-examples) to install the new version of Iris.
  You may have to do `make clean` as Coq will likely complain about .vo file
  mismatches.
